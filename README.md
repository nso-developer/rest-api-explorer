# REST API and RESTCONF Explorer

This is a tool for developers to explore all YANG models compiled
into NSO, with full editing capabilities using NSO northbound REST
APIs inculding RESTCONF. No YANG files are needed.

* Automatically build URL and payload templates for REST APIs directly by simply select the desired parameters
* Easy to "try it out" for all supported datastores and combinations of query parameters
* Automatic convert templates/payloads between JSON and XML
* Include important info from ncs_northbound-*.pdf document
* The YANG tree is automatically loaded from NCS: Each node type in YANG model has a dedicated icon in the TreeView.
* Clicking on each tree node, YANG info for the node is displayed automatically.
* Easy navigation using PathView or search

## How to install

Download file `rest-api-explorer.tar.gz`, a self-contained NSO package. 
NSO version 4.5 and later can use this package directly (see Option1 below); 
For NSO versions earlier than 4.5, see installation Option2 below.

#### Option1 (for NSO version 4.5 and later):
Copy file `rest-api-explorer.tar.gz` into your NSO running `packages` folder, then do NSO `package reload`. If successfull, you should see `rest-api-explorer` package at the bottom of NSO GUI. 

Note: if you are using NSO 5.3 and later, special care is needed in `ncs.conf`. See `Troubleshooting` section below.

![this](./rest-api-explorer-package.png)

#### Option2 (for *all* NSO versions):
```
tar zxvf rest-api-explorer.tar.gz
cp -r rest-api-explorer/webui $NCS_DIR/var/ncs/webui/docroot/rest-api-explorer
```

## Getting Started

On Chrome, enter the normal NSO UI URL, e.g.,
http://127.0.0.1:8080/ and login with your username and password.
If you use install Option1, simply click `rest-api-explorer` package in the Application Hub;
For Option2, enter http://127.0.0.1:8080/rest-api-explorer/ . 

The default username/password used in the tool is admin/admin. If your NSO login is different, then use the `Login` button at the top-right corner. Sometimes browser may prompt you to enter username/password: if this happens, click `Cancel` to close it, then click `Login` button as shown below:

![this](./login-browser.png) &nbsp;&nbsp;&nbsp;&nbsp; ![this](./login-option.png)

The main page of REST API Explorer should look like below. 
For more details, please see [user guide](./userguide.pdf).

![this](./screenshot.png)

## Troubleshooting

If the tree does not show up in TreeView (especially if for NSO 5.3 and later), make sure the following lines show up in `<webui>` section of  `ncs.conf`:

```
<webui>
  ...
  <custom-headers>
    <header>
      <name>Content-Security-Policy</name>
      <value>
        default-src 'self';
        font-src 'self' data:;
        img-src 'self' data:;
        style-src 'self' 'unsafe-inline' 'unsafe-eval';
        script-src 'self' 'unsafe-inline' 'unsafe-eval';
      </value>
    </header>
  </custom-headers>
</webui>
```

For NSO 5.3 and later, make sure rest-api-explorer option setting is consistent with your ncs.conf setting:
```
<restconf>
  ...
  <require-module-name>
    <enabled>false</enabled>
  </require-module-name>
</restconf>
````

## Known issue

For NSO 5.2 and later, some trees may not expand, e.g., device/config/ios:interface or device/live-status/ios-stats:exec.
NSO ticket `[Tail-f TAC #42110] JSON RPC Query to Devices tree not working in NSO 5.3.2` has been opened.
As a workaround, additional logic is built into the tool. To trigger this logic, all required is to define at least one device of desired type.

The workaround logic is disabled by default (for backward compatibility), which can be turned on manually using `Options` button,
by entering the name of any device of desired type.
Note that all options are reset to default if you do a hard browser refresh, so remember to set options again after refresh.

![this](./options.png)

If any device is selected, it is displayed in the top bar:

![this](./options-device.png)

Device must be selected before clicking any tree node, since after a tree node is expanded, it will not change. 
If you want to work on a different device type, use browser refresh.

## Note

The tool has been only tested on Chrome. Other browsers may 
produce unexpected results. 

The trace-id commit parameter was introduced in NSO 5.5. It can be used in NSO 5.5 and later . In order to use the trace-id , you need to enable it in the ncs.conf. 

```
/ncs-config/logs/trace-id (boolean) [true]
  Enable a per request unique trace id, included in headers and entries for relevant logs  
```

```
<logs>
  <trace-id>true</trace-id>
</logs>
```
You can specify a trace-id up to 64 character or let NSO generate a trace-id that will be present in header of the RESTCONF response that you got. The header name is X-Cisco-NSO-Trace-ID.

The with-service-meta-data parameter can only be used to get service metadata in NSO 5.6 and later. It can be used only for URL /restconf/data

