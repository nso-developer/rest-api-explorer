function TOOL_REST() { return true; }
function TOOL_YANG() { return !TOOL_REST();}

var contentBuf = '';
function myOut2(wnd,act) {
    if (act === 'end') {
        $('#' + wnd).html(contentBuf);
        return null;
    }

    if (act === 'start') {
        contentBuf = '';
        $('#' + wnd).html(contentBuf);
    }

    return function(content) {
        contentBuf += content;
    }
}

function myOut(wnd) {
    return function(content) {
        $('#' + wnd).html(content);
    }
}

function clearFrms(frm) {
    return myOut("swaggerPane");
}

function cleanUpTh() {
    window.sessionON = false;
    window.th_now = 0;
    window.deviceList = [];
}

function my_log(msg) {
    console.log(msg);
}

function jsonMsgClear() {
    parent.jmsgpane.document.body.innerHTML = '';
}

function jsonMsg(msg, data) {
    return;

    //var my_log = document.write.bind(parent.jmsgpane.document);	
    //my_log("<pre>");

    my_log(msg);
    if (data) my_log("  data: " + JSON.stringify(data, null, 2));

    //my_log("</pre><hr>");
    //if (msg.indexOf("comet")<0 && msg.indexOf("TimeOut")<0)
    //    parent.jmsgpane.scrollBy(0,100000);    
}

var tid = 0;
function JsonRpc(mthod, parms, doneCB, failCB) {

    function getJSON(params) {
        return JSON.stringify({
            jsonrpc: '2.0',
            id: tid,
            method: mthod,
            params: params
        }, null, 2);
    }

    var jsonStr = '';
    if (Array.isArray(parms)) {
        jsonStr = '['
        for (var i=0; i< parms.length; i++) {
            tid = tid + 1;
            if (i>0) jsonStr += ',';
            jsonStr += getJSON(parms[i]);
        }
        jsonStr += ']';
    } else {
        tid = tid + 1;
        jsonStr = getJSON(parms);
    }

    if (mthod !== 'comet')
        jsonMsg("SEND: " + jsonStr);

    var ajaxReq = {
        type: 'post',
        url: '/jsonrpc',
        contentType: 'application/json',
        data: jsonStr,
        timeout: 5000,
        dataType: 'json'
    };

    // ajaxReq['xhrFields'] = {withCredentials: true} ; //CORS

    $.ajax(ajaxReq).done(function (data) {
        jsonMsg("RECV: ", data);
        if (data.error && data.error.type === "session.invalid_sessionid") {
            SessionTimeout(data);
        } else {
            doneCB(data);
        }
    }).fail(function (err) {
        if (mthod !== 'comet') jsonMsg("RERR: ", err);

        if (failCB) failCB(err);
    }).always(function () {
        //jsonMsg("RECV: always");
    });

    return tid;
}

function SessionTimeout(data) {
    //alert("Session timed out. Refresh to login again.");
    //cleanUpTh();

    var myout = myOut('yangPane');
    myLogin();
    myout("Session timed out, a new session has been automatically started.");
}

function newReadTrans(doneCB) {
    JsonRpc('new_read_trans', {
        db: 'running'
    }, function (data) {
        doneCB(data);
    });
}

function newWriteTrans(doneCB) {
    JsonRpc('new_write_trans', {
        db: 'running',
        conf_mode: 'private'
    }, function (data) {
        doneCB(data);
    });
}

function newWebuiTrans(doneCB) {
    JsonRpc('new_webui_trans', {
        db: 'running'
    }, function (data) {
        doneCB(data);
    });
}

function start_heart_beat(cometId) {
    JsonRpc('comet', {
        comet_id: cometId
    }, function (data) {
        //jsonMsg("HBOK:",data);
        if (!data.error) start_heart_beat(cometId);
    }, function (err) {
        //jsonMsg("HBTO:",err);
        start_heart_beat(cometId);
    });
}

var login = { username: 'admin', password: 'admin' };

function getUsernamePasswd() {
    //console.log('------login-----', login)
    return login;
}

function myLoginClick() {
    login = {
        username: $("#username").val(),
        password: $("#password").val()
    };

    myLogin();
}

function myLogin() {
    cleanUpTh();
    clearFrms(1);

    //JsonRpc('get_trans',{},function(data) {});
    //JsonRpc("delete_trans",{th: 1},function(data) {});

    var login = getUsernamePasswd();

    JsonRpc('login', {
        user: login.username,
        passwd: login.password,
    }, function (data) {

        if (!data.result) { alert("Login failed for " + login.username); return; }

        window.sessionON = true;

        var th = window.th_now;
        if (th === undefined || th < 1) {
            newWebuiTrans(function (data) {
                window.th_now = data.result.th;
                window.buildTree();
                //start_heart_beat('heartbeat');
            });
        }
    });

    fillMyDeviceList();
}

function myLogout() {
    cleanUpTh();
    clearFrms(1);

    JsonRpc('logout', {
    }, function (data) {
        window.th_now = 0;

        alert("Logged out: refresh the page to login again");
    });
}

function get_my_th() {
    var th = window.th_now;

    if (th === undefined || th < 1) {
        alert("Login first");
        return 0;
    }

    return th;
}

function discard_th_now() {
    var th = get_my_th();
    if (th < 1) return;

    if (!confirm("Revert transaction " + th + "?")) {
        if (confirm("Logout now ?")) myLogout();

        return;
    }

    var myout = clearFrms(1);

    JsonRpc("delete_trans", {
        th: th
    }, function (data) {
        myout("Result of delete_trans:<pre>" + JSON.stringify(data, null, 2) + "</pre>");
        jsonMsg('Reverted transaction ' + th);

        newWebuiTrans(function (data) {
            window.th_now = data.result.th;
        });
    });
}

function validateCommit() {
    if (TOOL_REST()) {
        swaggerExport();
        return;
    }

    var th = get_my_th(); if (th < 1) return;

    var myout = clearFrms(1);

    if (!confirm("Commit changes now?")) {
        discard_th_now();
        return;
    }

    JsonRpc("validate_commit", {
        th: th
    }, function (data) {
        myout("Result of validate_commit:<pre>" + JSON.stringify(data, null, 2) + "</pre>");

        if (data.error) {
            //alert(JSON.stringify(data.error));
            return;
        }

        JsonRpc("commit", {
            th: th
        }, function (data) {
            myout("<br>Results of commit:<pre>" + JSON.stringify(data, null, 2) + "</pre>");

            if (data.error) {
                //alert(JSON.stringify(data.error));
                return;
            } else {
                //alert("Commit OK");

                newWebuiTrans(function (data) {
                    window.th_now = data.result.th;
                });
            }
        });
    });
}

function newTable(rows, cols, defVal) {
    var arr = [];

    for (var i = 0; i < rows; i++) {
        arr[i] = [];
        for (var j = 0; j < cols; j++) arr[i][j] = defVal;
    }

    return arr;
}

function cloneTable(arr, src) {
    for (var i = 0; i < src.length; i++) {
        if (i < arr.length) {
            for (var j = 0; j < src[i].length; j++) {
                if (j < arr[i].length && src[i][j]) arr[i][j] = src[i][j];
            }
        }
    }

    return arr;
}

function done_table(myout) {
    myout("</table>");

    myout("<style>table{border-collapse:collapse;} table,td{border:1px solid black;}</style>");
}

function on_focus_inputbox(inputbox) {
    inputbox.oldval = inputbox.value;
}

function my_set_value(combo, inputbox) {
    var th = get_my_th();
    if (th < 1) return;

    combo = JSON.parse(decodeURIComponent(combo));

    //my_log("my_set_value:"+JSON.stringify(combo,null,2));

    var path = combo.path;

    var val = inputbox.value;

    if (val === inputbox.oldval) return;

    var cmd = 'set_value';
    var args = { th: th, path: path, value: val };

    if (combo.type === "combo") {
        cmd = 'create';
        args = { th: th, path: path + "/" + val };
    }

    JsonRpc(cmd, args, function (data) {
        if (data.error) {
            alert(JSON.stringify(data.error));
            inputbox.value = inputbox.oldval;
        }
    });
}

function on_right_menu(menu) {
    var act = menu.value;

    if (act === "Commit") validateCommit();
    else if (act === "Revert") discard_th_now();
    else if (act === "Logout") myLogout();

    menu.value = "ActionMenu";
}

function isEmptyObject(obj) {
    for (var name in obj) return false;

    return true;
}
