function delete_all_roots(tr) {
    var children = tr.get_children_dom("#");
    for (var i = children.length - 1; i >= 0; i--) {
        var ch = tr.get_node(children[i]);

        if (ch.original && ch.original.rootShortCut === undefined)
            tr.delete_node(ch);
    }
}

function my_plugins() {
    var out = ["wholerow", "contextmenu", "types", "search"];

    if (!TOOL_YANG()) {
        out.push("checkbox");
    }

    return out;
}

function my_node(parent, text) {
    return { id: text, parent: parent, text: text, li_attr: { class: "hidebox" }, rootShortCut: true };
}

function my_tree() {
    var tree = [];

    return my_tree1(tree);
}

function my_tree1(tree) {
    if (TOOL_REST()) {
        tree.push(my_node("#", "/api"));
        tree.push(my_node("/api", "/api/running"));
        tree.push(my_node("/api/running", "/api/running/{operation}"));

        tree.push(my_node("/api", "/api/config"));
        tree.push(my_node("/api/config", "/api/config/{operation}"));

        tree.push(my_node("/api", "/api/operational"));
        tree.push(my_node("/api/operational", "/api/operational/{operation}"));

        tree.push(my_node("/api", "/api/rollbacks"));
        tree.push(my_node("/api/rollbacks", "/api/rollbacks/{filename}"));
        tree.push(my_node("/api/rollbacks", "/api/running/_rollback"));
        tree.push(my_node("/api/rollbacks", "/api/config/_rollback"));
        tree.push(my_node("/api/rollbacks", "/api/operational/_rollback"));
    }

    tree.push(my_node("#", "ncs:devices"));
    tree.push(my_node("#", "ncs:services"));
    //tree.push(my_node("#","vb:vbranch"));
    tree.push(my_node("#", "ncs:packages"));

    /*
    tree.push(my_node("#","ncs:cluster"));
    tree.push(my_node("#","tcm:ha-cluster"));
    tree.push(my_node("#","ncs:customers"));
    tree.push(my_node("#","ncs:java-vm"));
    tree.push(my_node("#","ncs:compliance"));
    tree.push(my_node("#","al:alarms"));   
    tree.push(my_node("#","ncs:ssh"));
    tree.push(my_node("#","ncs:snmp-notification-receiver"));
    tree.push(my_node("#","ncs:software"));
    tree.push(my_node("#","pnp:pnp"));
    tree.push(my_node("#","pnp:pnp-state"));
    tree.push(my_node("#","pnp:pnp-subscriber"));     
    */

    return tree;
}

var _models = [];
function getFullName(prefix, namesp, fullname) {
    for (var i = 0; i < _models.length; i++) {
        var model = _models[i];

        if (fullname !== null) {
            if (model.name === fullname) return model.namespace;

        } else {
            if (model.prefix === prefix || model.namespace === namesp)
                return model.name;
        }
    }

    return null;
}

function getModelNamespace(prefix) {
    for (var i = 0; i < _models.length; i++) {
        var model = _models[i];

        if (model.prefix === prefix)
            return model.namespace;
    }

    return null;
}

function buildTree() {
    //alert("mytool = " + parent.mytool);

    var tr = $('#myTree').jstree(true);

    newReadTrans(function (data) {
        var th = data.result.th;

        JsonRpc('get_system_setting', {
            operation: "all"
        }, function (data) {

            if (!data.result) {
                alert("get_system_setting failed");
                return;
            }

            //alert(JSON.stringify(data.result.models,null,2));

            delete_all_roots(tr);

            _models = data.result.models;

            var all = "#"; //tr.create_node("#", {text: "ALL MODELS",root2:true,icon:"fa fa-book"});

            var roots = [], cnt=0;
            for (var ns in _models) {
                cnt++;
                roots.push({
                    th: th,
                    namespace: _models[ns].namespace,
                    levels: 0,
                    evaluate_when_entries: true
                });

                if ((cnt % 1000) === 0) {
                    get_schema_chunk(roots, tr, all);
                    roots = [];
                    cnt = 0;
                }
            }

            if (roots.length != 0) get_schema_chunk(roots, tr, all);
        });
    });
}

function get_schema_chunk(roots, tr, all) {

    JsonRpc("get_schema", roots, function (dataArr) {
        for (var i = 0; i < dataArr.length; i++) {
            var data2 = dataArr[i];

            var chd = data2.result.data.children;
            var label = getFullName(null, data2.result.meta.namespace, null);

            if (chd.length > 0 && !label.startsWith("http://tail-f.com/ns/mibs/")) {
                var pnode = tr.create_node(all, { text: label, root2: true, icon: "fa fa-star-o" });
                procSchema(tr, pnode, data2, true);
                tr.close_node(pnode);
            }
        }
    });
}

function delete_all_children(tr, sel, excludeText) {
    var children = tr.get_children_dom(sel);
    for (var i = children.length - 1; i >= 0; i--) {
        var ch = tr.get_node(children[i]);

        if (excludeText === undefined || ch.text !== excludeText)
            tr.delete_node(ch);
    }
}

function is_instance(str) {
    return str !== undefined && str && str.length > 0 && str.charAt(0) === '{';
}

function get_instances(tr, node) {
    var cnt = 0;
    var children = tr.get_children_dom(node);
    for (var i = 0; i < children.length; i++) {
        var ch = tr.get_node(children[i]);

        var txt = tr.get_text(ch);

        if (is_instance(txt)) cnt++;
    }

    return cnt;
}

function get_child_node(tr, node, idx) {
    if (!node) return null;

    var children = tr.get_children_dom(node);
    if (children === undefined || children === null) return null;

    if (idx < 0 || idx >= children.length) return null;

    return tr.get_node(children[idx]);
}

function on_loaded_tree(e, data) {
    myLogin();
    refresh_top_bar();
}

function on_my_callback(act, e, data) {
    var tr = data.instance;
    var node = data.node;
    //my_log(act + " : " + tr.get_text(node));

    show_yang_info(tr, node);
}

function on_check_node(e, data) { on_my_callback("check_node", e, data); }
function on_uncheck_node(e, data) { on_my_callback("uncheck_node", e, data); }
function on_select_node(e, data) { on_my_callback("select_node", e, data); }
function on_deselect_node(e, data) { on_my_callback("deselect_node", e, data); }
function on_activate_node(e, data) { on_my_callback("activate_node", e, data); }

var _myTree = null;
var _myLastNode = null;
function on_changed_tree(e, data) {
    if (!window.sessionON) {
        alert("Session timed out. Use refresh button to start a new session.");
        return;
    }

    var tr = data.instance;
    var node = data.node;

    if (!node) return; //may happen on deselect_all

    //my_log("on_changed_tree:" + JSON.stringify(node,null,1));
    //my_log("on_changed_tree ["+tr.get_text(node)+"] ACTION="+data.action);

    refresh_top_bar(tr, node);

    processNode(tr, node, data);

    _myTree = tr;
    _myLastNode = node;
}

function processNode(tr, node, data) {
    clearFrms(1);
    //console.log('---processNode---',node,data);

    var kind = get_node_kind(node, tr);

    if (node && kind != 'leaf' && kind != 'leaf-list' &&
        //(!node.original || !node.original.gotSchema) && 
        (!node.children || node.children.length < 1) &&
        (!node.original || !node.original.root2) &&
        (!show_yang_info(tr, node) || !node.original.clicked)) {
        get_schema(tr, node, data);
    } else {
        processClick(tr, node);
    }

    tr.open_node(node);

    if (node && node.original) node.original.clicked = true;
}

var apiPrefix = "/restconf/data";

function on_api_prefix(combo) {
    apiPrefix = combo.value;

    var tr = $('#myTree').jstree(true);
    var sel = tr.get_selected(true);
    if (sel.length < 1) return;

    processClick(tr, sel[0]);
}

function IsRestconf() {
    return apiPrefix == "/restconf/data" || apiPrefix == "/restconf/operations" ; 
}

function IsRestconfData() {
    return apiPrefix == "/restconf/data" ;
}

function processClick(tr, node) {

    var kind = get_node_kind(node, tr);

    if (TOOL_REST()) {
        //if (kind === 'list') update_list_keys(tr, node);

        swaggerNode(tr, node, apiPrefix);
        return;
    }

    if (kind === 'list') {
        update_list_keys(tr, node);
    } else if (kind === 'action') {
        //apply_action(tr,node);
    } else if (/* kind==='key' || */ kind === 'leaf' || kind === 'leaf-list' || kind === 'choice') {
        get_leaf_value(tr, node, function (data, combo) {
            var myout = myOut2("swaggerPane", 'start');
            myout(tr.get_text(node) + ":&nbsp" + my_table_text(combo));
            myOut2("swaggerPane", 'end');
        });
    } else {
        show_table_row(tr, node);
    }
}

function get_leaf_nodes(tr, node) {
    var out = [];

    var checked = tr.get_children_dom(node);

    for (var i = 0; i < checked.length; i++) {
        var nd = tr.get_node(checked[i]);
        var kd = get_node_kind(nd, tr);

        if (kd === 'key' || kd === 'leaf' || kd === 'leaf-list' || kd === 'choice')
            out.push(nd);
    }

    return out;
}

function show_table_row(tr, node) {
    var myout = myOut2("swaggerPane",'start');

    //var nodes = get_leaf_nodes(tr,node);
    var nodes = show_edit_rows(tr, node);

    var nvals = nodes.length;

    var out = new Array(nvals);

    for (var i = 0, got = 0; i < nodes.length; i++) {
        var nd = tr.get_node(nodes[i]);
        var kd = get_node_kind(nd, tr);

        get_leaf_value(tr, nd, function (data, combo) {
            out[combo.col] = combo;

            if (++got >= nvals) {
                myout("<table>");

                for (var j = 0; j < nvals; j++) {
                    myout("<tr>");
                    myout("<td>" + out[j].text + "</td><td>" + my_table_text(out[j]) + "</td>");
                    myout("</tr>");
                }

                done_table(myout);
                myOut2("swaggerPane",'end');
            }
        }, null, i);
    }
}

function show_config(tr, node) {
    var th = get_my_th(); if (th < 1) return;

    var kind = get_node_kind(node, tr);

    var path = get_path(tr, node);

    //if (!confirm("Apply the following command?\n\n"+path)) return;

    var myout = document.write.bind(parent.editor.document);

    var jstr = {
        th: th,
        path: path
    };

    myout("<pre>action:" + JSON.stringify(jstr, null, 2) + "</pre>");

    JsonRpc('show_config', jstr, function (data) {
        myout("<pre>RECV: " + JSON.stringify(data, null, 2) + "</pre>");
    });
}

function apply_action(tr, node) {
    var th = get_my_th(); if (th < 1) return;

    var kind = get_node_kind(node, tr);
    if (kind !== 'action') {
        alert("Kind is not action");
        return;
    }

    var path = get_path(tr, node);

    //if (!confirm("Apply the following command?\n\n"+path)) return;

    var myout = document.write.bind(parent.editor.document);

    var jstr = {
        th: th,
        path: path,
        format: "normal"
    };

    if (path === "/ncs:packages/ncs:reload") {
        apply_action_must_read(myout, jstr);
    } else {
        apply_action_jstr(myout, jstr);
    }
}

function apply_action_jstr(myout, jstr) {
    myout("<pre>action:" + JSON.stringify(jstr, null, 2) + "</pre>");

    JsonRpc('action', jstr, function (data) {
        myout("<pre>RECV: " + JSON.stringify(data, null, 2) + "</pre>");
    });
}

function apply_action_must_read(myout, jstr) {
    //myout("<pre> delete_trans th = "+th+"</pre>");
    JsonRpc("delete_trans", {
        th: jstr.th
    }, function (data) {

        newReadTrans(function (data) {
            jstr.th = data.result.th;
            myout("<pre>action:" + JSON.stringify(jstr, null, 2) + "</pre>");

            JsonRpc('action', jstr, function (data) {
                myout("<pre>RECV: " + JSON.stringify(data, null, 2) + "</pre>");

                newWebuiTrans(function (data) {
                    window.th_now = data.result.th;
                });
            });
        });
    });
}

function show_yang_info(tr, node) {
    var myout = myOut('yangPane');

    var yang = get_yang(node, tr);
    if (!yang) {
        myout('');
        return false;
    }

    var out = '<pre>';

    out += JSON.stringify(yang, null, 2) + '\n\n';

    var tnd = tr.get_node(node);

    if (tnd.original && tnd.original.meta)
        out += JSON.stringify(tnd.original.meta, null, 2) + '\n\n' ;

    out += '</pre>';

    myout(out);
    return true;
}

function get_rest_keys(tr, node) {
    var keys = [];

    var kind = get_node_kind(node, tr);

    var pnd = tr.get_parent(node);
    if (!pnd) return keys;

    var pkind = get_node_kind(pnd, tr);
    if (pkind !== 'list')
        return keys;

    var children = tr.get_children_dom(pnd);
    for (var i = 0; i < children.length; i++) {
        var ch = tr.get_node(children[i]);
        var knd = get_node_kind(ch, tr);
        if (knd !== 'key') continue;

        keys.push(ch);
    }

    return keys;
}

function get_path(tr, node) {
    var path = tr.get_text(node);

    var leafentry = is_instance(path);

    for (var nd = tr.get_parent(node); nd; nd = tr.get_parent(nd)) {
        var txt = tr.get_text(nd);
        if (!txt) break;

        var tnd = tr.get_node(nd);
        if (tnd.original && tnd.original.root2) break;

        var kind = get_node_kind(nd, tr);
        if (kind === 'choice' || kind === 'case') continue;

        if (!leafentry) path = "/" + path;
        leafentry = is_instance(txt);

        path = txt + path;
    }

    return "/" + path;
}

function rest_path_param(node, tr) {
    var yang = get_yang(node, tr);
    if (!yang) return null;

    var name = yang.name;
    var desc = yang.info ? yang.info.string : null;

    var prt = tr.get_parent(node);
    if (prt) {
        var py = get_yang(prt, tr);
        if (py) {
            //name =  py.name + "_" + yang.name ;
            name = yang.name + node.parents.length;

            if (!desc) desc = yang.name + " of " + py.qname;
        }
    }

    return $.extend({
        in: "path",
        name: name,
        description: desc,
        required: true
    },
        swagger_type(yang));
}

var includeModuleName = false;
function myOptionsClick() {

    if (_myLastNode) {
        alert('Change of any global options must be done before\nclicking any tree node.\n\n' +
            'After a subtree is expanded, it will not change.\n\n' +
            'Click browser Refresh button now, then open options\ndialog immediately.\n'
        );
    }

    includeModuleName = $("#includeModuleName").is(':checked');
}

function rest_text(tr, node, topBar) {
    var txt = tr.get_text(node);
    if (txt[0] === '/') return [txt, []];

    var yang = get_yang(node, tr);
    if (!yang) {
        var txt = tr.get_text(node);

        var i = txt.lastIndexOf(":");

        return [txt.substring(i + 1), []];
    }

    var txt = yang.name; //tr.get_text(node);

    if (!txt) return ['',[]]; //module root node such as 'tailf-aaa'

    if (includeModuleName) {
        txt = tr.get_text(node);
        txt = ModifyNodeName(txt);
    }

    var kind = get_node_kind(node, tr);

    if (kind === 'action' && !IsRestconf()) txt = "_operations/" + txt;

    var params = [];

    var keys = get_rest_keys(tr, node);

    if (keys.length < 1) {
        var dup = 0;

        var prt = tr.get_parent(node);
        var children = tr.get_children_dom(prt);
        for (var i = 0; i < children.length; i++) {
            var ch = children[i];
            var y = get_yang(ch, tr);
            if (!y) continue;

            if (y.name === txt) dup++;
            if (dup > 1) break;
        }

        if (dup > 1) txt = yang.qname;

        return [txt, []];
    }

    var out = "";

    for (var i = 0; i < keys.length; i++) {
        if (i > 0 && !topBar) out += ",";

        var yang = get_yang(keys[i], tr);
        if (yang) {
            var rp = rest_path_param(keys[i], tr);

            if (!topBar) {
                out += "{" + rp.name + "}";
            }

            params.push(rp);
        }
    }

    if (IsRestconf() && !topBar) out = "=" + out;

    if (kind !== 'key') {
        if (!topBar) out += '/';

        out += txt;
    }

    return [out, params];
}

function refresh_top_bar(tr, node) {
    if (!tr || !node) return;

    var myout = myOut('restPath');
    var txt = tr.get_text(node);

    if (TOOL_REST()) {
        if (txt[0] === '/') {
            myout("<a href=# onclick=change_path('" + node.id + "')>" + rest_text(tr, node, true)[0] + "</a>");
            return;
        }

        txt = rest_text(tr, node,true)[0];
    }

    var path = "<a href=# onclick=change_path('" + node.id + "')>" + txt + "</a>";

    var leafentry = is_instance(tr.get_text(node));

    for (var nd = tr.get_parent(node); nd; nd = tr.get_parent(nd)) {
        var txt = tr.get_text(nd);
        if (!txt) break;

        var tnd = tr.get_node(nd);
        if (tnd.original && tnd.original.root2) break;

        var kind = get_node_kind(nd, tr);
        if (kind === 'choice' || kind === 'case') continue;

        if (!leafentry) path = "/" + path;
        leafentry = is_instance(txt);

        if (!TOOL_YANG()) txt = rest_text(tr, nd, true)[0];

        path = "<a href=# onclick=change_path('" + tr.get_node(nd).id + "')>" + txt + "</a>" + path;
    }

    myout("/" + path);
}

function change_path(ndId) {
    var trnd = scrollToNode(ndId);

    //processNode(trnd[0],trnd[1]);
}

function find_child(tr, node, text) {
    var children = tr.get_children_dom(node);

    var ret = null;

    for (var i = 0; i < children.length; i++) {
        var ch = tr.get_node(children[i]);

        var txt = tr.get_text(ch);

        if (txt === text) { ret = ch; break }
    }

    return ret;
}

function get_key_str(key, kind) {
    if (kind === 'leaf' || kind === 'leaf-list' || kind === 'choice') return key;

    var out = "";

    for (var i = 0; i < key.length; i++) {

        if (out.length > 0) out += " ";

        var str = key[i];

        if (str.indexOf(' ') >= 0) str = "'" + str + "'";

        out += str;
    }

    return '{' + out + '}';
}

function show_edit_rows(tr, node) {
    var checked = [];

    var pkind = get_node_kind(node, tr);

    if (pkind === 'leaf' || pkind === 'leaf-list' || pkind === 'choice') {
        checked.push(node);
        return checked;
    }

    checked = tr.get_children_dom(node);

    var out = [];

    for (var j = 0; j < checked.length; j++) {
        var nd = tr.get_node(checked[j]);

        var kind = get_node_kind(nd, tr);

        if (kind === 'leaf' || kind === 'leaf-list') {
            out.push(nd);

        } else if (kind === 'container') {

            var lev2 = tr.get_children_dom(nd);
            for (var k = 0; k < lev2.length; k++) {
                var nd2 = tr.get_node(lev2[k]);

                var kind2 = get_node_kind(nd2, tr);
                if ((kind2 === 'leaf' || kind2 === 'leaf-list' || kind2 === 'choice'))
                    out.push(nd2);
            }
        }
    }

    return out;
}

function show_editor(tr, node, keys) {
    if (!TOOL_YANG()) return;

    if (keys.length < 1 || get_my_th() < 1) return;

    var nodes = show_edit_rows(tr, node);

    var rows = keys.length, cols = nodes.length;
    if (rows < 1 || cols < 1) return;

    clearFrms(1);
    var myout = myOut2("swaggerPane",'start');

    myout("<table>");

    myout("<tr>");
    myout("<td><pre>list-entry</pre></td>");
    for (var j = 0; j < nodes.length; j++) {
        var nd = tr.get_node(nodes[j]);

        var knd = get_node_kind(nd, tr);
        var txt = tr.get_text(nd);

        if (knd === 'key') txt = "<b>" + txt + "</b>";

        myout("<td>" + txt + "</td>");
    }
    myout("</tr>");

    for (var i = 0, got = 0; i < keys.length; i++) {
        get_table_row(tr, node, keys[i], nodes, function (cols) {
            myout("<tr>");
            for (var j = 0; j < cols.length; j++) myout("<td>" + cols[j] + "</td>");
            myout("</tr>");

            if (++got >= rows) {
                done_table(myout);
                myOut2("swaggerPane",'end');
            }
        });
    }
}

function get_table_row(tr, node, okey, nodes, callback) {

    var prefix = get_path(tr, node);
    var pkind = get_node_kind(node, tr);

    var key = get_key_str(okey, pkind);

    var keynd = find_child(tr, node, key);

    var pfix = prefix + key;

    var cols = nodes.length;

    var out = new Array(nodes.length + 1);

    out[0] = "<a href=# onclick=scrollToNode('" + keynd.id + "')>" + key + "</a>";

    for (var j = 0, got = 0; j < nodes.length; j++) {
        var nd = tr.get_node(nodes[j]);

        var kind = get_node_kind(nd, tr);
        var txt = tr.get_text(nd);

        var path = pfix;

        if (pkind !== 'list-entry') {
            var ndp = tr.get_parent(nd);//level2
            var ptxt = tr.get_text(ndp);
            if (tr.get_node(ndp) !== node)
                path += "/" + ptxt;

            if (kind !== 'choice' && kind !== 'case')
                path += "/" + txt;
        }

        get_leaf_value(tr, nd, function (data, combo) {
            var col = combo.col;
            out[col + 1] = my_table_text(combo);

            if (++got >= cols) callback(out);
        }, path, j);
    }
}

function get_leaf_value(tr, node, callback, nodepath, col) {
    var th = get_my_th(); if (th < 1) return 0;

    var path = nodepath ? nodepath : get_path(tr, node);

    var kind = get_node_kind(node, tr);

    var cmd = 'get_value';
    var args = { th: th, path: path };

    var combo = {};

    if (kind === 'choice') {
        path = nodepath ? nodepath : get_path(tr, tr.get_parent(node));
        var choice = tr.get_text(node);

        cmd = 'get_case';
        args = { th: th, path: path, choice: choice };

        var clist = [];
        for (var c = 0; c < node.original.yang.cases.length; c++) {
            var cv = node.original.yang.cases[c];
            clist.push(cv.name);
        }

        combo["path"] = path;
        combo["type"] = "combo";
        combo["choice"] = choice;
        combo["clist"] = clist;
    } else {
        combo["path"] = path;
        combo["type"] = "text";
    }

    combo["kind"] = kind;
    combo["node"] = node.id;
    combo["col"] = col;
    combo["text"] = tr.get_text(node);

    var tid = JsonRpc(cmd, args, function (data) {
        combo["value"] = get_data_val(data);

        callback(data, combo);
    });

    return { tid: tid, combo: combo };
}

function my_table_text(combo) {
    var tr = $('#myTree').jstree(true);
    var node = tr.get_node(combo.node);
    var yang = (node.original ? node.original.yang : null);

    var type = combo.type;
    var kind = combo.kind;
    var val = combo.value;

    var args = encodeURIComponent(JSON.stringify(combo));

    if (kind === 'key' || yang && yang.config === false) return val;

    if (type !== "combo") {
        return "<input onfocus=on_focus_inputbox(this) onblur=my_set_value(\"" + args + "\",this) type=text style='background:greenyellow' value=\"" + val + "\">";
    }

    var out = "<select onclick=on_focus_inputbox(this) onchange=my_set_value(\"" + args + "\",this)>";

    var selected = false;

    for (var i = 0; i < combo.clist.length; i++) {
        var opt = combo.clist[i];

        out += "<option value='" + opt + "'";

        if (opt === combo.value) {
            out += " selected";
            selected = true;
        }

        out += ">" + opt;
    }

    if (!selected) out += "<option value='-' selected>-";

    out += "</select>";

    return out;
}

function get_data_val(data) {
    return data.result ?
        data.result.case ? data.result.case :
            data.result.value ? data.result.value : "" : "";
}

function get_yang(node, tr) {
    var nd = tr ? tr.get_node(node) : node;

    return nd.original ? nd.original.yang : null;
}

function get_meta(node, tr) {
    var nd = tr ? tr.get_node(node) : node;

    return nd.original ? nd.original.meta : null;
}

function get_node_kind(node, tr) {
    var yang = get_yang(node, tr);

    return yang ? yang.kind : !node ? "" : node.original ?
        node.original.ykind ? node.original.ykind : "" : "";
}

var selectedDevice = '';
function onMyDevice(combo) {
    selectedDevice = (combo.value === '--' ? '' : combo.value);
    $('#selectedDevice').text(selectedDevice);
}

function fillMyDeviceList() {
    getDeviceList((devices) => {
        //console.log('---fillMyDeviceList---',devices);

        var myList = $("#myDeviceList");

        var options = '<option>--</option>';
        for (var i in devices) {
            var dev = devices[i];
            options += ('<option>' + dev.name + '</option>');
        }

        myList.html(options);
        myList.selectpicker('refresh');
    });
}

function getDeviceList(doneCB) {

    // Until NSO ticket [Tail-f TAC #42110] is resolved, at least one device is required to expand this tree

    $.ajax({
        type: "GET",
        url: "/restconf/data/tailf-ncs:devices/tailf-ncs:device?fields=name",
        dataType: 'json',
        headers: {
            'Authorization': "Basic " + btoa(login.username + ":" + login.password),
            'Accept': 'application/vnd.yang.collection+json'
        }
    }).done(function (data) {
        var devices = data.collection['tailf-ncs:device'];
        if (doneCB) doneCB(devices);
    });
}

function get_schema(tr, node, data) {
    var th = get_my_th();
    if (th < 1) return;

    var path = get_path(tr, node);

    var devName = selectedDevice;
    //console.log("---workarround test---[",devName,']',path);
    
    /* convert '/ncs:devices/ncs:device/ncs:config/ios:interface'
         into '/ncs:devices/ncs:device{DEV1}/ncs:config/ios:interface'
       convert '/ncs:devices/ncs:device/live-status/ios-stats:exec'
               '/ncs:devices/ncs:device/live-status' for ncs-5.5
    */
    if (devName && path && path.startsWith('/ncs:devices/ncs:device/')) {
        //console.log('---workarround devName-------', devName);
    
        var nodes = path.split('/');
        if (nodes.length >= 4) {
            path = '/ncs:devices/ncs:device{' + devName + '}';
            for (var i = 3; i < nodes.length; i++) path += '/' + nodes[i];
        }
    }

    get_schema1(tr, node, th, path, data);
}

function get_schema1(tr,node,th,path, data) {

    showLoading(data);
    JsonRpc('get_schema', {
        th: th,
        path: path,
        levels: 1,
        evaluate_when_entries: true

    }, function (schema) {

        procSchema(tr, node, schema);
        showLoading(null);
    });
}

function procSchema(tree, node, schema, skipOpen) {
    if (schema.result) {
        var data = schema.result.data;
        if (data) procSchemaJson(tree, node, data, schema.result.meta);
    }

    if (!skipOpen) tree.open_node(node);

    show_yang_info(tree, node);

    processClick(tree, node);
}

function delete_all_instances_not_in_keys(tr, node, keys) {
    var children = tr.get_children_dom(node);
    for (var i = children.length - 1; i >= 0; i--) {
        var ch = tr.get_node(children[i]);

        if (!is_instance(ch.text)) continue;

        var found = false;
        for (var k = 0; k < keys.length; k++) {
            var text = '{' + keys[k].join(' ') + '}';

            if (text === ch.text) {
                found = true;
                break;
            }
        }

        if (found) continue;

        my_log("Deleting key: " + ch.text);
        tr.delete_node(ch);
    }
}

function update_list_keys(tree, node) {
    var kind = get_node_kind(node, null);

    if (kind !== 'list') return;

    //my_log('update_list_keys');

    var th = get_my_th();
    if (th < 1) return;

    JsonRpc('get_list_keys', {
        th: th,
        path: get_path(tree, node),
        chunk_size: 100
    }, function (data) {
        if (!data.result) return;

        var keys = data.result.keys;
        my_log("KEYS:" + keys);

        delete_all_instances_not_in_keys(tree, node, keys);

        var keynds = [];

        for (var i = 0; i < keys.length; i++) {
            var text = get_key_str(keys[i], kind);

            var ch = find_child(tree, node, text);

            if (ch) keynds.push(ch);
            else keynds.push(create_list_entry(tree, node, text));
        }

        show_editor(tree, node, keys);
    });
}

function create_list_entry(tree, node, text) {
    var info = { text: text };

    //info.li_attr = {class : "hidebox greenyellow" };
    info.icon = "fa fa-asterisk";

    info.ykind = 'list-entry';

    var ch = tree.create_node(node, info);
    //my_log("Added key: "+text);

    return ch;
}

function highlightLabel(tree, ch, text) {
    var info = { text: text };

    if (ch.kind === 'key') info.li_attr = { class: "bold" };
    else if (ch.kind === 'action') info.li_attr = { class: "italic" };

    var icons = {
        "module": "fa-book",
        "access-denies": "fa-ban-circle",
        "list-entry": "fa-asterisk",
        "choice": "fa-cutlery",
        "key": "fa-paperclip",
        "leaf-list": "fa-tasks",
        "action": "fa-wrench",
        "container": "fa-folder-o",
        "leaf": "fa-leaf",
        "list": "fa-th-list",
        "notification": "fa-bell"
    };

    for (var kind in icons) {
        if (ch.kind === kind) {
            info.icon = "fa " + icons[kind];
            break;
        }
    }

    return info;
}

function procSchemaJson(tree, node, data, meta) {
    var tnd = tree.get_node(node);

    if (tnd.original) {
        var orig = tnd.original;

        if (!orig.yang) orig.yang = remember_yang(data);

        if (meta) orig.meta = meta;

        orig.gotSchema = true;
    }

    var children = data.children;
    if (!children) return;

    for (var i = 0; i < children.length; i++) {
        var ch = children[i];

        var text = ch.qname;
        if (text === undefined) text = ch.name;

        var info = highlightLabel(tree, ch, text);
        info.yang = remember_yang(ch);

        var nd = tree.create_node(node, info);

        //var kind = get_node_kind(nd,tree);
        //if (kind==='leaf' || kind==='choice') tree.hide_icon(nd);
        //tree.check_node(nd);

        if (ch.kind === 'choice') {
            for (var j = 0; j < ch.cases.length; j++) {
                var ch2 = ch.cases[j];
                var name = ch2.name;

                var info = { text: name };
                info.yang = remember_yang(ch2);

                var nd2 = tree.create_node(nd, { text: name, icon: "fa fa-arrow-right" });
                procSchemaJson(tree, nd2, ch2, meta);
            }
        } else {
            procSchemaJson(tree, nd, ch, meta);
        }
    }
}

function remember_yang(original) {
    if (!original) return undefined;

    var yang = {};

    for (var name in original) {
        if (name !== "children") yang[name] = original[name];
    }

    return yang;
}

var searching = false;

function menuItems(node) {
    var tr = $('#myTree').jstree(true);

    if (TOOL_REST()) return {};

    return {
        "Add": {
            "label": "Add new list-entry",
            "separator_before": false,
            "action": function (data) { add_new_list_entry(tr, node); }
        },
        "Delete": {
            "label": "Delete list-entry",
            "separator_after": true,
            "action": function (data) { delete_list_entry(tr, node); }
        },
        "Action": {
            "label": "Action",
            "separator_after": false,
            "action": function (data) { apply_action(tr, node); }
        },
        "ShowConfig": {
            "label": "Show Config",
            "separator_after": false,
            "action": function (data) { show_config(tr, node); }
        },
        "Subscribe": {
            "label": node.original['subhndl'] ? "UnSubscribe Changes" : "Subscribe Changes",
            "separator_before": true,
            "action": function (data) { subscribeChange(tr, node); }
        }
        /*
        "Search": {
            "label": searching ? "Clear search" : "Search tree",
            "separator_after": true,
            "action": function(data) {search_tree(tr,node);}
        },
        "UnCheckAll": {
            "label": "Uncheck all nodes",
            "action": function(data) {tr.uncheck_all();}
        },       
        "CheckAll": {
            "label": "Check all nodes",
            "action": function(data) {tr.check_all();}
        } */
    };
}

function search_tree(tr, node) {
    /*
    if (searching) {
        tr.clear_search();
        searching = false;
        return;
    }
    */

    var text = prompt("Enter node label:", "");
    if (!text || text.length < 1) return;

    tr.search(text /*,true,true*/);
    //searching = true;
}

function on_search() {
    search_tree2($('#mySearch').val());
}

function search_tree2(text) {
    var tr = $('#myTree').jstree(true);
    tr.search(text);
}

function on_search_tree(e, data) {
    var tr = $('#myTree').jstree(true);

    clearFrms(1);

    var myout = myOut2('yangPane','start');

    for (var i = 0; ; i++) {
        var results = $(this).find('.jstree-search:eq(' + i + ')');
        if (!results || results.length < 1)
            break;

        var nd = results[0];
        var label = get_path(tr, nd);

        var jnd = tr.get_node(nd);

        myout("<a href=# onclick=scrollToNode('" + jnd.id + "')>" + label + "</a><br>");

        //if (i === 0) nd.scrollIntoView();
    }

    myOut2('yangPane','end');
}

function scrollToNode(ndId) {
    if (!ndId) return;

    var tr = $('#myTree').jstree(true);
    var nd = tr.get_node(ndId);
    if (!nd) return;

    tr.deselect_all();

    tr.select_node(nd);

    var nd2 = document.getElementById(ndId);

    if (nd2) nd2.scrollIntoView();

    return [tr, nd];
}

function add_new_entry() {
    var tr = $('#myTree').jstree(true);
    var sels = tr.get_selected(true);

    if (sels.length > 0) {
        var nd = sels[0];
        if (!confirm("Add new entry to\n" + get_path(tr, nd) + " ?")) return;

        add_new_list_entry(tr, nd);
    }
}

function add_new_list_entry(tr, node) {
    var text = tr.get_text(node);
    var path = get_path(tr, node);
    var kind = get_node_kind(node, tr);

    if (kind !== 'list') {
        alert("Node " + text + " is not a list:\nCan not create new entry.");
        return;
    }

    var th = get_my_th(); if (th < 1) return;

    var keys = [];

    var children = tr.get_children_dom(node);
    for (var i = 0; i < children.length; i++) {
        var ch = tr.get_node(children[i]);
        var knd = get_node_kind(ch, tr);
        if (knd !== 'key') continue;

        var name = tr.get_text(ch);

        var key = prompt("Enter key " + name + ":", "");
        if (!key || key.length < 1) {
            alert("Invalid " + name);
            return;
        }

        keys.push(key);
    }

    var keystr = get_key_str(keys, kind);
    var pname = path + keystr;

    JsonRpc("exists", {
        th: th,
        path: pname
    }, function (data) {

        if (!data.result) { alert("Invalid format"); return; }

        if (data.result.exists) {
            alert(pname + "already exists. Can not create!");
            return;
        }

        JsonRpc('create', {
            th: th,
            path: pname
        }, function (data) {
            if (!data.result) { alert("Invalid format"); return; }
            else {
                //var entry = create_list_entry(tr,node,keystr);
                //tr.select_node(entry);
                update_list_keys(tr, node);
            }
        });
    });
}

function delete_node_entry(ndId, callback) {
    var tr = $('#myTree').jstree(true);
    var nd = tr.get_node(ndId);

    delete_list_entry(tr, nd, callback);
}

function delete_list_entry(tr, node, callback) {
    var text = tr.get_text(node);
    var kind = get_node_kind(node, tr);
    var path = get_path(tr, node);

    if (!is_instance(text)) {
        alert("Selected node is not a list-entry!");
        return;
    }

    if (!confirm("Delete the following list-entry?\n\n" + path)) return;

    var th = get_my_th(); if (th < 1) return;

    var tpnd = tr.get_parent(node);

    JsonRpc("delete", {
        th: th,
        path: path
    }, function (data) {
        if (data.result) {
            tr.delete_node(node);
            if (callback) callback(tr, tpnd);
        }
    });
}

function show_my_cmds() {
    clearFrms(1);
    var myout = document.write.bind(parent.editor.document);

    myout("<table>");

    my_table_row(myout, '<a href=# onclick="discard_th_now()">Revert</a>', 'Discard current transaction');

    my_table_row(myout, '<a href=# onclick="validateCommit()">Commit</a>', 'Validate and commit');

    my_table_row(myout, '<a href=# onclick="myLogout()">Logout</a>', ' ');

    //my_table_row(myout,'<a href=# onclick="myLogin()">Login</a>',      ' ');

    myout("</table>");
}

function my_table_row(myout, col1, col2) {
    myout("<tr><td>" + col1 + "</td> <td>" + col2 + "</td></tr>");
}

function sendComet(tr, node) {
    var cometId = node.id;

    JsonRpc('comet', {
        comet_id: cometId
    }, function (data) {

        var str = "RECEIVED NOTIFICATION:\n" + JSON.stringify(data, null, 1);

        clearFrms(1);
        var my_log = document.write.bind(parent.editor.document);
        my_log("<pre><span class='inner-pre' style='font-size:14px;background:yellow'>" + str + "</span></pre>");

        if (!data.error && node && node.original['subhndl'])
            sendComet(tr, node);

    }, function (err) {

        if (err.error && !err.error.type && node && node.original['subhndl'])
            sendComet(tr, node);
    });
}

function subscribeChange(tr, node) {
    if (node.original['subhndl']) {
        unsubscribeChanges(tr, node);
        delete node.original['subhndl'];
    } else {
        subscribeChanges(tr, node);
    }
}

function subscribeChanges(tr, node) {
    var cid = node.id;
    var path = get_path(tr, node);

    JsonRpc('subscribe_changes', {
        comet_id: cid,
        path: path,
        handle: cid

    }, function (data) {

        var str = JSON.stringify(data, null, 1);

        clearFrms(1);
        var my_log = document.write.bind(parent.editor.document);
        my_log("<pre>" + str + "</pre>");
        my_log("Subscribed changes: " + get_path(tr, node));

        jsonMsg('Subscribed handle = [' + data.result.handle + ']');

        if (data.result && data.result.handle)
            node.original['subhndl'] = data.result.handle;

        sendComet(tr, node);
    });

    return cid;
}

function unsubscribeChanges(tr, node) {
    JsonRpc('unsubscribe', {
        handle: node.original['subhndl']
    }, function (data) {
        var str = JSON.stringify(data, null, 1);

        clearFrms(1);
        var my_log = document.write.bind(parent.editor.document);
        my_log("<pre>" + str + "</pre>");
        my_log("Subscription removed: " + get_path(tr, node));
    });
}

function get_def_model(tr, node, includeKey) {
    var kind = get_node_kind(node, tr);
    var yang = get_yang(node, tr);

    var prNd = tr.get_node(tr.get_parent(node));
    var pknd = get_node_kind(prNd, tr);

    var nd = node;
    var kd = kind;

    if (kind === 'key' && pknd === 'list') {
        nd = prNd;
        kd = pknd;
    }

    var txt = tr.get_text(nd);
    txt = ModifyNodeName(txt);

    var json = loopSubTree(tr, nd, includeKey ? true : kind === 'list');

    if (nd === prNd) //for 'PUT'
    {
        return [merge_model(txt, json, {}, kd, true), false];

    } else if (kind === 'leaf' || kind === 'leaf-list') {

        var out = {};
        out[txt] = swagger_type(yang);

        return [out, true];

    } else if (kind === 'action') {

        return [merge_model("input", json, {}, kd, false), false];
    }

    return [json, true];
}

function get_definitions(tr, node) {
    var def =
    {
        "ModelPatch": { "type": "object" },
        "ModelDefault": { "type": "object" }
    };

    var patch = get_def_model(tr, node, false);
    var deflt = get_def_model(tr, node, true);

    def.ModelPatch["properties"] = patch[0];
    def.ModelDefault["properties"] = deflt[0];

    var config = { "name": "config", "namespace": "http://tail-f.com/ns/config/1.0" };

    if (patch[1]) def.ModelPatch["xml"] = config;
    else def.ModelPatch["xml"] = { "name": "RemoveThisTagXML" };

    if (deflt[1]) def.ModelDefault["xml"] = config;
    else def.ModelDefault["xml"] = { "name": "RemoveThisTagXML" };

    /******
    def = {
        "ModelDefault": {
            "type": "object",
            "properties": {
                "group1": {
                    "type": "object",
                    "properties": { "param": { "type": "string" } }
                }
            },
            "xml": { "name": "RemoveThisTagXML" }
        }
    };
    /*****/

    return def;
}

function loopSubTree(tr, node, includeKey) {
    var out = {};

    var children = tr.get_children_dom(node);

    for (var i = 0; i < children.length; i++) {
        var ch = tr.get_node(children[i]);
        var kind = get_node_kind(ch, tr);
        var txt = tr.get_text(ch);

        txt = ModifyNodeName(txt);

        var yang = get_yang(ch, tr);

        if (kind === 'key' && !includeKey) continue;

        if (kind === 'key') {
            if (tr.is_checked(ch)) {
                out[txt] = swagger_type(yang);
            }

        } else if (kind === 'leaf' || kind === 'leaf-list') {
            if (tr.is_checked(ch)
                && (!yang || yang.access === undefined || yang.access.create || yang.access.update)
                && (!yang || yang.config === undefined || yang.config)
            ) {
                out[txt] = swagger_type(yang);
            }

        } else {
            var json = loopSubTree(tr, ch, true);
            if (!isEmptyObject(json)) {
                out = merge_model(txt, json, out, kind, true);
            }
        }
    }

    return out;
}

function merge_model(txt, json, out, kind, addXML) {

    if (kind === 'choice') {
        return $.extend(out, json);
    }

    var obj = { "type": "object" };
    var prop = { "properties": json };

    if (kind === 'list') {
        obj = {
            type: "array",
            items: {
                type: "object",
                properties: json
            }
        };

        prop = {};
    }

    var tmp = {};
    tmp[txt] = $.extend(obj, prop);

    if (addXML) {
        var i = txt.indexOf(":");
        if (i >= 0) {
            var prefix = txt.slice(0, i);
            var ns = getFullName(null, null, prefix);
            var name = txt.slice(i + 1, txt.length);

            tmp[txt]['xml'] = { name: name, namespace: ns, prefix: prefix };
        }
    }

    return $.extend(out, tmp);
}

function swagger_type(yang) {
    if (!yang) return { type: "string" };

    if (yang.kind === "leaf-list") {
        return { type: "array", items: { type: "string" } };
    }

    if (!yang.type || !yang.type.name) return { type: "string" };

    var nm = yang.type.name;

    if (nm === "boolean") {
        var val = false;

        return { type: "boolean", default: val };
    }

    if (nm.indexOf("counter") >= 0 ||
        nm.indexOf("gauge") >= 0 ||
        nm.indexOf("int") >= 0 ||
        nm.indexOf("number") >= 0) {
        return { type: "integer" };
    }

    return { type: "string" };
}

function ModifyNodeName(txt) {
    return ModifyNodeName1(txt)[0];
}

function ModifyNodeName1(txt) {
    var i = txt.indexOf(":");
    if (i < 0) return [txt,''];

    var prefix = txt.slice(0, i);

    var full = getFullName(prefix, null, null);
    if (full) return [full + txt.slice(i, txt.length), prefix];

    return [txt, ''];
}

function buildTemplate() {
    var tr = $('#myTree').jstree(true);
    var sel = tr.get_selected(true);
    if (sel.length < 1) return;
    var nd = sel[0];

    //var out = loopSubTree(tr,nd);
    var out = get_definitions(tr, nd);

    var myout = myOut('yangPane');
    myout("<pre>" + JSON.stringify(out, null, 2) + "</pre>");
}

function showLoading(data) {
    var loading = document.getElementById('loadingMark');

    if (data && data.event && data.event.pageX && data.event.pageY) {
        var x = data.event.pageX;
        var y = data.event.pageY;
        
        loading.style.left = x+100;
        loading.style.top = y;

        loading.style.display = "block";
    } else {
        loading.style.display = "none";
    }
}

function loadingSwagger(show) {
    var loading = document.getElementById('loadingSwagger');
    loading.style.display = (show ? "block" : "none");
}
